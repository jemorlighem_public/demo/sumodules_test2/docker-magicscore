FROM python:3.9

WORKDIR /usr/src/app

# Install libraries and configure REST API 
RUN pip install flask \
                flask-restful
ENV SERVICE_PORT=5000

# Install Git and clone remote repository 
RUN apt-get update
RUN apt-get install -y git
ARG SERVICE_GIT_REPOSITORY=https://gitlab.com/jemorlighem_public/demo/sumodules_test2/service-magicscore.git
RUN git clone --recurse-submodules $SERVICE_GIT_REPOSITORY service

COPY README ./README
COPY api.py ./api.py

LABEL service_name="magicscore"
LABEL authors="Jean-Etienne Morlighem <jemorlighem@vegetalsignals.com>"
LABEL description="Docker to illustrate the use of Git submodules"