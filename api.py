
# author: "Jean-Etienne Morlighem <jemorlighem@vegetalsignals.com>"
# date: 2021-11


import os
from flask import Flask, request
from flask_restful import Resource, Api

from service import service


SERVICE_PORT = os.environ["SERVICE_PORT"]


app = Flask(__name__)
api = Api(app)


class Readme(Resource):
    def get(self):
        with open("README", "r") as f:
            readme = f.read()
        return readme


class Compute(Resource):
    def post(self):
        payload = request.get_json()
        result = service.compute(payload)
        return result


api.add_resource(Readme, "/readme")
api.add_resource(Compute, "/compute")


if __name__ == "__main__":
    app.run(host = "0.0.0.0", port = SERVICE_PORT)
